from django.views import generic
from website.models import Item
from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
import datetime
from .forms import RezerwujItemForm, ContactForm
from django.views.generic.detail import DetailView


class ItemView(generic.ListView):
    queryset = Item.objects.filter(czy_wypozyczony=False)
    template_name = "website/itemIndex.html"
    context_object_name = "object_list"
    def get_context_data(self, *, object_list=context_object_name, **kwargs):
        context = super().get_context_data(**kwargs)
        context['wypozyczone_list'] = Item.objects.filter(czy_wypozyczony=True)
        return context


class ItemDetailView(DetailView):
    model = Item
    template_name = 'website/itemDetail.html'


def profile(request):
    items = Item.objects.filter(wlasciciel=request.user.id)
    dt = datetime.datetime.utcnow()
    return render(request, 'website/profile.html', {'items': items, 'date': dt})


def wypozycz(request, pk,):
    item = Item.objects.get(id=pk)
    item.wlasciciel = request.user
    item.save()
    return redirect('/accounts/profile/')


def zarezerwuj_item(request, pk):
    item = get_object_or_404(Item, id=pk)
    calendar = datetime
    # Jesli POST request to dane do form
    if request.method == 'POST':

        #Stworz forme i wypelnij informacjami z modelu
        form = RezerwujItemForm(request.POST)

        #Sprawdz czy forma jest poprawna
        if form.is_valid():
            item.data_oddania = form.cleaned_data['data_oddania']
            item.data_wypozyczenia = form.cleaned_data['data_wypozyczenia']
            item.czy_wypozyczony = True
            item.wlasciciel = request.user
            ilosc_dni = item.data_oddania - item.data_wypozyczenia
            item.do_zaplaty = ilosc_dni.days * item.cena
            item.save()

            return redirect('/accounts/profile/')
    # Jesli GET request to
    else:
        proponowana_data_wypozyczenia = datetime.date.today()
        proponowana_data_oddania = datetime.date.today() + datetime.timedelta(days=10)
        form = RezerwujItemForm(initial={'data_oddania': proponowana_data_oddania, 'data_wypozyczenia': proponowana_data_wypozyczenia})

    return render(request, 'website/itemDetailRezerwuj.html', {'form': form, 'item': item})


def o_nas(request):
    return render(request, 'website/oNas.html')


def contact_form(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            pass
    else:
        form = ContactForm()
    return render(request, "website/contact_form.html", {'form': form})
