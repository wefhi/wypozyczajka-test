# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-06-12 12:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0002_auto_20180407_2146'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='opis',
            field=models.TextField(default='Opis przedmiotu. Jeśli to widzisz, zmodyfikuj opis przedmiotu przez panel administratora.'),
        ),
    ]
