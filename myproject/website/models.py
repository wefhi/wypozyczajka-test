from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User #one-to-one technique user
from django.db.models.signals import post_save #one-to-one technique user
from django.dispatch import receiver #one-to-one technique user



class Item(models.Model):
    nazwa = models.CharField(max_length=100)
    cena = models.IntegerField(null=True, blank=True)
    zdjecie = models.FileField()
    wlasciciel = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)
    czy_wypozyczony = models.BooleanField(default=False)
    data_oddania = models.DateField(null=True, blank=True)
    data_wypozyczenia = models.DateField(null=True, blank=True)
    do_zaplaty = models.IntegerField(null=True, blank=True)
    opis = models.TextField(default="Opis przedmiotu. Jeśli to widzisz, zmodyfikuj opis przedmiotu przez panel administratora.")
    minimalny_okres = models.IntegerField(default="0")
    kaucja = models.IntegerField(null=True, blank=True)
    nowosc = models.BooleanField(default=False)    
    promocja = models.BooleanField(default=False)
    bez_kaucji = models.BooleanField(default=False)

    def get_absolute_url(self):
        return reverse('website:itemdetail', kwargs={'pk': self.pk})

    def update_wlasciciel(self):
        self.wlasciciel = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.nazwa 
