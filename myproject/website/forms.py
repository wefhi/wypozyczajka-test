from django import forms

from django.contrib.admin.widgets import AdminDateWidget

class DatePicker(forms.DateInput):
  input_type = "date"


class RezerwujItemForm(forms.Form):
    data_wypozyczenia = forms.DateField(widget=DatePicker
                                        )
    data_oddania = forms.DateField(widget=DatePicker)


class ContactForm(forms.Form):
    imię = forms.CharField(max_length=30)
    email = forms.EmailField(max_length=254)
    wiadomość = forms.CharField(
        max_length=2000,
        widget=forms.Textarea(),
        help_text="Wpisz tu swoją wiadomość"
    )
    source = forms.CharField(
        max_length=50,
        widget=forms.HiddenInput()
    )

    def clean(self):
        cleaned_data = super(ContactForm, self).clean()
        imię = cleaned_data.get("imię")
        email = cleaned_data.get("email")
        wiadomość = cleaned_data.get("wiadomość")

        if not imię and not email and not wiadomość:
            raise forms.ValidationError("Musisz coś napisać!")
