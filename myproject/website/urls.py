from django.conf.urls import url
from . import views
from django.http import HttpResponse, HttpRequest
from django.views.i18n import JavaScriptCatalog


app_name = 'website'

urlpatterns = [

    url(r'^$', views.ItemView.as_view(), name='item-view'),
    url(r'items/$', views.ItemView.as_view(), name='item-view'),
    url(r'^items/(?P<pk>[0-9]+)/$', views.ItemDetailView.as_view(), name="itemdetail"),
    url(r'^item/(?P<pk>[0-9]+)/$', views.zarezerwuj_item, name="rezerwujitem"),
    url(r'^accounts/profile/', views.profile, name='profile'),
    url(r'^items/update/(?P<pk>[0-9]+)/$', views.wypozycz, name="wypozycz"),
    url(r'^/items/update/profile/', views.profile, name='profile'),
    url(r'o-nas/$', views.o_nas, name='o-nas'),
    url('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog'),
    url(r'contact_form', views.contact_form, name='contactform'),
]
